**Android Kotlin Insert Data to Database with Firebase**

Description:

In this tutorial, we gonna write code how to insert data to Firebase Realtime Database.

Step:

Login into google from android studio

```
click icon user at top right page in Android Studio > Login to your account
```

Add Firebase to your project

```
Tools > Firebase > Realtime Database > Login > Connect to Firebase > Add Firebase to project
```

firebase Realtime Database rules without login:
````
service firebase.storage {
  match /b/{bucket}/o {
    match /{allPaths=**} {
      //allow read, write: if request.auth != null;
      allow read, write;
    }
  }
}

````

There's 2 file in this project:
1. MainActivity
2. activity_main.xml

Code in MainActivity

````
package com.example.ahmadkarimh.firebaseinsert

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var database= FirebaseDatabase.getInstance()
    private var myRef=database.reference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun buInsert(view: View){
        try {
            myRef.child("People").child("1").child("name").setValue(etName.text.toString())
            myRef.child("People").child("1").child("age").setValue(etAge.text.toString())
            Toast.makeText(applicationContext, "inserted " + etName.text.toString(), Toast.LENGTH_SHORT).show()

        }catch(e: Exception){
            Toast.makeText(applicationContext, e.message.toString(), Toast.LENGTH_SHORT).show()

        }


    }

}

````

Code in activity_main.xml

````
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <EditText
        android:id="@+id/etName"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginTop="56dp"
        android:ems="10"
        android:hint="Name"
        android:inputType="textPersonName"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.502"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <EditText
        android:id="@+id/etAge"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginTop="56dp"
        android:ems="10"
        android:hint="Age"
        android:inputType="textPersonName"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.502"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/etName" />

    <Button
        android:id="@+id/button2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginTop="68dp"
        android:onClick="buInsert"
        android:text="Insert"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/etAge" />
</android.support.constraint.ConstraintLayout>
````