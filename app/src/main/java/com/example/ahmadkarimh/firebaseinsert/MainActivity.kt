package com.example.ahmadkarimh.firebaseinsert

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var database= FirebaseDatabase.getInstance()
    private var myRef=database.reference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun buInsert(view: View){
        try {
            myRef.child("People").child("1").child("name").setValue(etName.text.toString())
            myRef.child("People").child("1").child("age").setValue(etAge.text.toString())
            Toast.makeText(applicationContext, "inserted " + etName.text.toString(), Toast.LENGTH_SHORT).show()

        }catch(e: Exception){
            Toast.makeText(applicationContext, e.message.toString(), Toast.LENGTH_SHORT).show()

        }


    }

}
